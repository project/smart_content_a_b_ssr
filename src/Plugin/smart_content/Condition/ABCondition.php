<?php

namespace Drupal\smart_content_a_b_ssr\Plugin\smart_content\Condition;

use Drupal\smart_content\Condition\ConditionTypeConfigurableBase;

/**
 * Provides a default Smart Condition.
 *
 * @SmartCondition(
 *   id = "ab",
 *   label = @Translation("A/B"),
 *   group = "ab_ssr",
 *   weight = 0,
 *   deriver = "Drupal\smart_content_a_b_ssr\Plugin\Derivative\ABConditionDerivative"
 * )
 */
class ABCondition extends ConditionTypeConfigurableBase {

  /**
   * A/B letter for current request.
   *
   * @var string
   */
  private static $letter;

  /**
   * {@inheritdoc}
   */
  public function getLibraries() {
    $libraries = array_unique(array_merge(parent::getLibraries(), ['smart_content_a_b_ssr/condition.ab']));
    return $libraries;
  }

  /**
   * {@inheritdoc}
   */
  public function getAttachedSettings() {
    $settings = parent::getAttachedSettings();
    $field_settings = $settings['settings'] + $this->defaultFieldConfiguration();
    $derivative_id = $this->getDerivativeId();

    // Whether or not condition passes.
    $ab_value = FALSE;

    switch ($derivative_id) {
      case 'ab':
        if (!isset(self::$letter)) {
          // Count for number of segments in the set.
          $seg_count = 2;

          if ($seg_count) {
            $min = ord('A');
            $max = $min + $seg_count - 1;
            // Choose randomly whether condition is passed.
            self::$letter = chr(rand($min, $max));
          }
        }
        if (isset($field_settings['value'])) {
          // Does random letter match condition's letter.
          $ab_value = (self::$letter == $field_settings['value']);
        }
        break;
    }

    // Set ab settings to be used in JS.
    $field_settings['ab_ssr'] = [
      'value' => $ab_value,
    ];

    // Set field condition settings.
    $settings['settings'] = $field_settings;
    $settings['field']['settings'] = $field_settings;

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultFieldConfiguration() {
    return [
      'ab_ssr' => [
        'derivative' => NULL,
        'value' => NULL,
      ],
    ];
  }

}
