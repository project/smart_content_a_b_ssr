<?php

namespace Drupal\smart_content_a_b_ssr\Plugin\smart_content\Condition\Group;

use Drupal\smart_content\Condition\Group\ConditionGroupBase;

/**
 * Provides a Smart Condition Group.
 *
 * @SmartConditionGroup(
 *   id = "ab_ssr",
 *   label = @Translation("A/B SSR"),
 *   weight = 0,
 * )
 */
class ABSSR extends ConditionGroupBase {

}
