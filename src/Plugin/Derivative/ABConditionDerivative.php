<?php

namespace Drupal\smart_content_a_b_ssr\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Deriver for ABCondition.
 *
 * Provides a deriver for
 * Drupal\smart_content_a_b_ssr\Plugin\smart_content\Condition\ABCondition.
 */
class ABConditionDerivative extends DeriverBase {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = [
      'ab' => [
        'label' => $this->t('A/B'),
        'type' => 'array_select',
        'options_callback' => [get_class($this), 'getAlphabetOptions'],
      ] + $base_plugin_definition,
    ];
    return $this->derivatives;
  }

  /**
   * Returns list of 'Interest' options for select element.
   *
   * @return array
   *   Array of Interest tids.
   */
  public static function getAlphabetOptions() {
    $options = [];

    // Create array of letter options.
    for ($i = ord('A'); $i <= ord('B'); $i++) {
      $options[chr($i)] = chr($i);
    }

    return $options;
  }

}
