/**
 * @file
 * Provides condition values for all browser conditions.
 */

 (function (Drupal) {

  Drupal.smartContent = Drupal.smartContent || {};
  Drupal.smartContent.plugin = Drupal.smartContent.plugin || {};
  Drupal.smartContent.plugin.Field = Drupal.smartContent.plugin.Field || {};

  // Handle A/B conditions based on settings.
  Drupal.smartContent.plugin.Field['ab_ssr'] = function (condition) {
    return condition.settings.ab_ssr.value ?? null;
  }

})(Drupal);
